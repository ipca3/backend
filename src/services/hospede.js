const bcrypt = require('bcrypt-nodejs');
const ValidationError = require('../errors/validationError');

module.exports = (app) => {
  const findAll = (filter = {}) => {
    return app.db('hospedes').where(filter).select(['id_utilizador', 'name', 'telefone', 'email']);
  };

  const findOne = (filter = {}) => {
    return app.db('hospedes').where(filter).first();
  };

  const getPasswdHash = (passwd) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(passwd, salt);
  };

  const save = async (hospede) => {
    if (!hospede.name) throw new ValidationError('Nome é um atributo obrigatório');
    if (!hospede.telefone) throw new ValidationError('Telefone é um atributo obrigatório');
    if (!hospede.email) throw new ValidationError('O email é um atributo obrigatório');
    if (!hospede.password) throw new ValidationError('Password obrigatória');

    const hospedeDb = await findOne({ email: hospede.email });
    if (hospedeDb) throw new ValidationError('Email duplicado na BD');

    const newHospede = { ...hospede };
    newHospede.password = getPasswdHash(hospede.password);
    return app.db('hospedes').insert(newHospede, ['id_utilizador', 'name', 'telefone', 'email']);
  };

  return {
    findAll, findOne, getPasswdHash, save,
  };
};
