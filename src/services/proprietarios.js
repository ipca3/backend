const bcrypt = require('bcrypt-nodejs');
const ValidationError = require('../errors/validationError');

module.exports = (app) => {
  const findAll = (filter = {}) => {
    return app.db('proprietarios').where(filter).select(['id_proprietario', 'nome', 'email', 'telefone', 'password', 'alojamento']);
  };

  const findOne = (filter = {}) => {
    return app.db('proprietarios').where(filter).first();
  };

  const getPasswdHash = (password) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
  };

  const save = async (proprietarios) => {
    if (!proprietarios.nome) throw new ValidationError('Nome é um atributo obrigatório');
    if (!proprietarios.email) throw new ValidationError('O email é um atributo obrigatório');
    if (!proprietarios.telefone) throw new ValidationError('Telefone é um atributo obrigatório');
    if (!proprietarios.password) throw new ValidationError('Password obrigatória');
    if (!proprietarios.alojamento) throw new ValidationError('O Alojamento é um atributo obrigatório');

    const proprietariosDb = await findOne({ email: proprietarios.email });
    if (proprietariosDb) throw new ValidationError('Email duplicado na BD');

    const newproprietarios = { ...proprietarios };
    newproprietarios.password = getPasswdHash(proprietarios.password);
    return app.db('proprietarios').insert(newproprietarios, ['id_proprietario', 'nome', 'email', 'telefone', 'password', 'alojamento']);
  };

  return {
    findAll, findOne, getPasswdHash, save,
  };
};
