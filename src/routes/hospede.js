module.exports = (app) => {
  const hospedeService = app.services.hospede;

  const create = async (req, res, next) => {
    try {
      const result = await hospedeService.create(req.body);
      return res.status(201).json(result[0]);
    } catch (err) {
      return next(err);
    }
  };

  const get = async (req, res, next) => {
    const { id } = req.params;

    try {
      const result = await hospedeService.get(id);
      return res.status(200).json(result);
    } catch (err) {
      return next(err);
    }
  };

  return {
    create, get,
  };
};
