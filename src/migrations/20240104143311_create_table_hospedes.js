exports.up = (knex) => {
  return knex.schema.createTable('hospedes', (t) => {
    t.increments('id_utilizador').primary();
    t.string('name').notNull();
    t.integer('telefone').notNull();
    t.string('email').notNull().unique();
    t.string('password').notNull();
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('hospedes');
};
