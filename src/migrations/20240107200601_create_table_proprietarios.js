exports.up = (knex) => {
  return knex.schema.createTable('proprietario', (t) => {
    t.increments('id_proprietario').primary();
    t.integer('email').notNull();
    t.string('nome').notNull();
    t.string('telefone').notNull().unique();
    t.string('password').notNull();
    t.string('alojamento').notNull();
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('proprietario');
};
