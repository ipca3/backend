module.exports = (app) => {
  app.route('/hospede').post(app.routes.hospede.create);
  app.route('/proprietarios').post(app.route.proprietarios.create);

  app.route('/hospede')
    .get(app.routes.hospede.list)
    .post(app.routes.hospede.create);

  app.route('/proprietarios')
    .get(app.routes.proprietarios.list)
    .post(app.routes.proprietarios.create);

  app.route('/hospede/:id')
    .get(app.routes.hospede.get)
    .put(app.routes.hospede.update)
    .delete(app.routes.hospede.remove);

  app.route('/proprietarios/:id')
    .get(app.routes.proprietarios.get)
    .put(app.routes.proprietarios.update)
    .delete(app.routes.proprietarios.remove);
};
