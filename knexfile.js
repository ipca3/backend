module.exports = {
    test: {
        client: 'pg',
        connection: {
            host: 'localhost',
            port: '9080',
            user: 'postgres',
            password: 'password',
            database: 'alojamento_temporario',
        },
        debug: true,
        migrations: {
            directory: 'src/migrations',
        },
        pool: {
            min: 1,
            max: 50,
            propagateCreateError: false,
        },
    },
};