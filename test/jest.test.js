test('Validar as principais operaçãoes do JEST', () => {
  let number = null;
  expect(number).toBeNull();
  number = 10;
  expect(number).not.toBeNull();
  expect(number).toBe(10);
  expect(number).toEqual(10);
  expect(number).toBeGreaterThan(9);
  expect(number).toBeLessThan(11);
});

test('Validar operações com objetos', () => {
  const obj = { name: 'Dinis Silva', mail: 'a24835@alunos.ipca.pt' };
  expect(obj).toHaveProperty('name');
  expect(obj).toHaveProperty('name', 'Dinis Silva');
  expect(obj.name).toBe('Dinis Silva');

  const obj2 = { name: 'Dinis Silva', mail: 'a24835@alunos.ipca.pt' };
  expect(obj).toEqual(obj2);
  // expect(obj).toBe(obj2);
});
