const request = require('supertest');
const jwt = require('jwt-simple');
const app = require('../../src/app');
const proprietarioService = require('../../src/services/proprietarios');

const secret = 'ipca!DWM202122';
let user;

beforeAll(async () => {
  const res = await app.services.user.save({ name: 'Gil Marques', email: 'u12345@ipca.pt', password: '12345' });
  user = { ...res[0] };
  user.token = jwt.encode(user, secret);
});

beforeEach(async () => {
  await proprietarioService.deleteAll();
});

test('Test #1 - Listar os Proprietarios', () => {
  return request(app)
    .get('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(200);
      expect(res.body.length).toBeGreaterThan(0);
    });
});

test('Test #2 - Inserir Proprietarios', () => {
  return request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Gil Marques', telefone: 123456789, email: 'Gil@example.com', password: '12345',
    })
    .then((res) => {
      expect(res.status).toBe(201);
      expect(res.body.name).toBe('Gil Marques');
      expect(res.body).not.toHaveProperty('password');
    });
});

test('Test #2.1 - Guardar a senha encriptada do Proprietario', async () => {
  const res = await request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Gil Marques', telefone: 987654321, email: 'Gil@example.com', password: '12345',
    });
  expect(res.status).toBe(201);

  const { id } = res.body;
  const proprietariosDB = await app.services.proprietario.findOne({ id });
  expect(proprietariosDB.password).not.toBeUndefined();
  expect(proprietariosDB.password).not.toBe('12345');
});

test('Test #3 - Inserir Proprietario sem Nome', () => {
  return request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({ telefone: 123456789, email: 'proprietario@example.com', password: '12345' })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Nome é um atributo obrigatório');
    });
});

test('Test #4 - Inserir Proprietario sem Email', async () => {
  const result = await request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({ name: 'Gil Marques', telefone: 987654321, password: '12345' });
  expect(result.status).toBe(400);
  expect(result.body.error).toBe('O email é um atributo obrigatório');
});

test('Test #5 - Inserir Proprietario sem Senha', (done) => {
  request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({ name: 'Gil Marques', telefone: 123456789, email: 'proprietario@example.com' })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Senha obrigatória');
      done();
    });
});

test('Test #6 - Inserir Proprietarios Duplicado', () => {
  return request(app)
    .post('/proprietarios')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Gil Marques', telefone: 123456789, email: 'Gil@example.com', password: '12345',
    })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Email duplicado na BD');
    });
});
