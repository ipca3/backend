const request = require('supertest');
const jwt = require('jwt-simple');
const app = require('../../src/app');
const hospedeService = require('../../src/services/hospede');

const secret = 'ipca!DWM202122';
let user;

beforeAll(async () => {
  const res = await app.services.user.save({ name: 'Dinis Silva', email: 'u12345@ipca.pt', password: '12345' });
  user = { ...res[0] };
  user.token = jwt.encode(user, secret);
});

beforeEach(async () => {
  await hospedeService.deleteAll(); // Limpa a tabela de hóspedes antes de cada teste
});

test('Test #1 - Listar os Hóspedes', () => {
  return request(app)
    .get('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(200);
      expect(res.body.length).toBeGreaterThan(0);
    });
});

test('Test #2 - Inserir Hóspede', () => {
  return request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Dinis Silva', telefone: 123456789, email: 'Dinis@example.com', password: '12345',
    })
    .then((res) => {
      expect(res.status).toBe(201);
      expect(res.body.name).toBe('Guilherme Nogueira');
      expect(res.body).not.toHaveProperty('password');
    });
});
test('Test #2.1 - Guardar a senha encriptada do Hóspede', async () => {
  const res = await request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Dinis Silva', telefone: 987654321, email: 'dinis@example.com', password: '12345',
    });
  expect(res.status).toBe(201);

  const { id } = res.body;
  const hospedeDB = await app.services.hospede.findOne({ id });
  expect(hospedeDB.password).not.toBeUndefined();
  expect(hospedeDB.password).not.toBe('12345');
});

test('Test #3 - Inserir Hóspede sem Nome', () => {
  return request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({ telefone: 123456789, email: 'hospede@example.com', password: '12345' })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Nome é um atributo obrigatório');
    });
});

test('Test #4 - Inserir Hóspede sem Email', async () => {
  const result = await request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({ name: 'Dinis Silva', telefone: 987654321, password: '12345' });
  expect(result.status).toBe(400);
  expect(result.body.error).toBe('O email é um atributo obrigatório');
});

test('Test #5 - Inserir Hóspede sem Senha', (done) => {
  request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({ name: 'Dinis Silva', telefone: 123456789, email: 'hospede@example.com' })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Senha obrigatória');
      done();
    });
});

test('Test #6 - Inserir Hóspede Duplicado', () => {
  return request(app)
    .post('/hospedes')
    .set('authorization', `Bearer ${user.token}`)
    .send({
      name: 'Dinis Silva', telefone: 123456789, email: 'Dinis@example.com', password: '12345',
    })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Email duplicado na BD');
    });
});
